type t('value)
and observer('value) = {. "next": 'value => unit, "error": string => unit};

type unsubscribe = unit => unit;

[@bs.send] external subscribe' : (t('value), observer('value)) => unsubscribe = "subscribe";

let subscribe = (~next, ~error, observer) => subscribe'(observer, {"next": next, "error": error});
