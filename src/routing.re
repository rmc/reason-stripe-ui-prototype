[@bs.get] external location : Dom.window => Dom.location = "";

[@bs.get] external pathname : Dom.location => string = "";

[@bs.get] external hash : Dom.location => string = "";

[@bs.get] external search : Dom.location => string = "";

[@bs.get] external origin : Dom.location => string = "";

module type SwitchConfig = {
  type t;
  let defaultRoute: t;
  let matcher: (t => unit, t => unit, ReasonReact.Router.url) => unit;
  let toString: t => string;
};

module MakeSwitch = (SwitchConfig: SwitchConfig) => {
  open ReasonReact;
  type state =
    | NotInitialized
    | Initialized(SwitchConfig.t);
  type action =
    | Init
    | SetRoute(SwitchConfig.t)
    | Redirect(SwitchConfig.t);
  let component = reducerComponent("Routing.Switch");
  let make = (~render, _children) => {
    ...component,
    initialState: () => NotInitialized,
    didMount: ({send}) => {
      send(Init);
      NoUpdate
    },
    reducer: (action, _state: state) =>
      switch action {
      /* router watchUrl does not trigger on initial load and router does
         not expose the parse function so we push the current url on component
         did mount to match initial route  */
      | Init =>
        SideEffects(
          (
            (_) =>
              switch [%external window] {
              | None => ()
              | Some((window: Dom.window)) =>
                Router.push(
                  (window |> location |> pathname)
                  ++ (window |> location |> search)
                  ++ (window |> location |> hash)
                )
              }
          )
        )
      | SetRoute(route) => Update(Initialized(route))
      | Redirect(route) => SideEffects(((_) => route |> SwitchConfig.toString |> Router.push))
      },
    subscriptions: ({send}) => [
      Sub(
        () =>
          Router.watchUrl(
            SwitchConfig.matcher(
              (route) => send(SetRoute(route)),
              (route) => send(Redirect(route))
            )
          ),
        Router.unwatchUrl
      )
    ],
    render: ({state}) =>
      switch state {
      | NotInitialized => ReasonReact.nullElement
      | Initialized(route) => render(route)
      }
  };
};

module type LinkConfig = {type t; let toString: t => string;};

module MakeLink = (LinkConfig: LinkConfig) => {
  open ReasonReact;
  let component = statelessComponent("Routing.Link");
  let make = (~route, children) => {
    ...component,
    render: (_) =>
      createDomElement(
        "a",
        ~props={
          "href": "",
          "onClick": (event) => {
            ReactEventRe.Synthetic.preventDefault(event);
            route |> LinkConfig.toString |> Router.push
          }
        },
        children
      )
  };
};

module MakeRedirect = (LinkConfig: LinkConfig) => {
  open ReasonReact;
  type action =
    | Redirect;
  let component = reducerComponent("Routing.Redirect");
  let make = (~route, _children) => {
    ...component,
    initialState: () => (),
    didMount: ({send}) => {
      send(Redirect);
      NoUpdate
    },
    reducer: (action, _state: unit) =>
      switch action {
      | Redirect => SideEffects(((_) => route |> LinkConfig.toString |> Router.push))
      },
    render: (_) => nullElement
  };
};

module Routes = {
  type t =
    | Form(Donation.PaymentMethod.t)
    | Pending(
        string,
        [
          | `Simple
          | `Bitcoin(pendingBitcoinParams)
          | `ACHCreditTransfer(pendingACHCreditTransferParams)
        ]
      )
    | Redirect(redirectParams)
    | External(string)
  and pendingACHCreditTransferParams = {
    amount: int,
    routingNumber: string,
    accountNumber: string
  }
  and pendingBitcoinParams = {
    receiverAddress: string,
    bitcoinUri: string,
    bitcoinAmount: float
  }
  and redirectParams = {
    clientSecret: string,
    sourceId: string
  };
  let parseRedirectSearch = (search) => {
    let parsed: {. "client_secret": Js.Undefined.t(string), "source": Js.Undefined.t(string)} =
      QueryString.parse("?" ++ search);
    switch (Js.Undefined.to_opt(parsed##client_secret), Js.Undefined.to_opt(parsed##source)) {
    | (Some(clientSecret), Some(sourceId)) => Some({clientSecret, sourceId})
    | _ => None
    }
  };
  let stringifyRedirectSearch = ({clientSecret, sourceId}) =>
    {"client_secret": clientSecret, "source_id": sourceId} |> QueryString.stringify;
  let parseExternalSearch = (search) => {
    let parsed: {. "redirect_url": Js.Undefined.t(string)} = QueryString.parse("?" ++ search);
    switch (Js.Undefined.to_opt(parsed##redirect_url)) {
    | Some(redirectUrl) => Some(redirectUrl)
    | _ => None
    }
  };
  let stringifyExternalSearch = (redirectUrl) =>
    {"redirect_url": redirectUrl} |> QueryString.stringify;
  let parsePendingBitcoinSearch = (search) => {
    let parsed: {
      .
      "receiver_address": Js.Undefined.t(string),
      "bitcoin_uri": Js.Undefined.t(string),
      "bitcoin_amount": Js.Undefined.t(string)
    } =
      QueryString.parse("?" ++ search);
    switch (
      Js.Undefined.to_opt(parsed##receiver_address),
      Js.Undefined.to_opt(parsed##bitcoin_uri),
      Js.Undefined.to_opt(parsed##bitcoin_amount)
    ) {
    | (Some(receiverAddress), Some(bitcoinUri), Some(bitcoinAmountString)) =>
      switch (float_of_string(bitcoinAmountString)) {
      | bitcoinAmount => Some({receiverAddress, bitcoinUri, bitcoinAmount})
      | exception (Failure("float_of_string")) => None
      }
    | _ => None
    }
  };
  let stringifyPendingBitcoinSearch = ({receiverAddress, bitcoinUri, bitcoinAmount}) =>
    {
      "receiver_address": receiverAddress,
      "bitcoin_uri": bitcoinUri,
      "bitcoin_amount": bitcoinAmount
    }
    |> QueryString.stringify;
  let parsePendingACHCreditTransferSearch = (search) => {
    let parsed: {
      .
      "amount": Js.Undefined.t(string),
      "routing_number": Js.Undefined.t(string),
      "account_number": Js.Undefined.t(string)
    } =
      QueryString.parse("?" ++ search);
    switch (
      Js.Undefined.to_opt(parsed##amount),
      Js.Undefined.to_opt(parsed##routing_number),
      Js.Undefined.to_opt(parsed##account_number)
    ) {
    | (Some(amountString), Some(routingNumber), Some(accountNumber)) =>
      switch (int_of_string(amountString)) {
      | amount => Some({amount, routingNumber, accountNumber})
      | exception (Failure("int_of_string")) => None
      }
    | _ => None
    }
  };
  let stringifyPendingACHCreditTransferSearch = ({amount, routingNumber, accountNumber}) =>
    {"amount": amount, "routing_number": routingNumber, "account_number": accountNumber}
    |> QueryString.stringify;
  let toString = (route) =>
    switch route {
    | Form(paymentMethod) => "/" ++ Donation.PaymentMethod.toString(paymentMethod)
    | Redirect(params) => "/redirect?" ++ stringifyRedirectSearch(params)
    | Pending(donationId, `Simple) => "/pending/" ++ donationId
    | Pending(donationId, `Bitcoin(params)) =>
      "/pending/" ++ donationId ++ "/bitcoin?" ++ stringifyPendingBitcoinSearch(params)
    | Pending(donationId, `ACHCreditTransfer(params)) =>
      "/pending/"
      ++ donationId
      ++ "/ach_credit_transfer?"
      ++ stringifyPendingACHCreditTransferSearch(params)
    | External(params) => "/pending/external?" ++ stringifyExternalSearch(params)
    };
  let defaultRoute = Form(Donation.PaymentMethod.Card);
  let matcher = (match, redirect, {path, hash: _hash, search}: ReasonReact.Router.url) =>
    switch (path, search) {
    | (["redirect"], qs) =>
      switch (parseRedirectSearch(qs)) {
      | Some(params) => match(Redirect(params))
      | _ => defaultRoute |> redirect
      }
    | (["pending", "external"], qs) =>
      switch (parseExternalSearch(qs)) {
      | Some(params) => match(External(params))
      | _ => defaultRoute |> redirect
      }
    | (["pending", donationId, "bitcoin"], qs) =>
      switch (parsePendingBitcoinSearch(qs)) {
      | Some(params) => match(Pending(donationId, `Bitcoin(params)))
      | _ => defaultRoute |> redirect
      }
    | (["pending", donationId, "ach_credit_transfer"], qs) =>
      switch (parsePendingACHCreditTransferSearch(qs)) {
      | Some(params) => match(Pending(donationId, `ACHCreditTransfer(params)))
      | _ => defaultRoute |> redirect
      }
    | (["pending", donationId], _) => Pending(donationId, `Simple) |> match
    | ([paymentMethodParam], _) =>
      switch (paymentMethodParam |> Donation.PaymentMethod.ofString) {
      | None => defaultRoute |> redirect
      | Some(paymentMethod) => Form(paymentMethod) |> match
      }
    | _ => defaultRoute |> redirect
    };
};

module Switch = MakeSwitch(Routes);

module Link = MakeLink(Routes);

module Redirect = MakeRedirect(Routes);

let linkHome = <Link route=Routes.defaultRoute> (ReasonReact.stringToElement("go back")) </Link>;
