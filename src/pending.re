let component = ReasonReact.statelessComponent("Pending");

let make =
    (
      ~donation,
      ~subscriptionState,
      ~type_: [
         | `Simple
         | `Bitcoin(Routing.Routes.pendingBitcoinParams)
         | `ACHCreditTransfer(Routing.Routes.pendingACHCreditTransferParams)
       ],
      _children
    ) => {
  ...component,
  render: (_) =>
    <div>
      (ReasonReact.stringToElement("donation state: " ++ donation##state))
      <pre> (ReasonReact.stringToElement("subscription state: " ++ subscriptionState)) </pre>
      (
        switch type_ {
        | `Simple => ReasonReact.nullElement
        | `ACHCreditTransfer({amount, routingNumber, accountNumber}) =>
          <div>
            <h4> (ReasonReact.stringToElement("Routing Number")) </h4>
            (ReasonReact.stringToElement(routingNumber))
            <h4> (ReasonReact.stringToElement("Account Number")) </h4>
            (ReasonReact.stringToElement(accountNumber))
            <h4> (ReasonReact.stringToElement("Amount")) </h4>
            (ReasonReact.stringToElement(string_of_int(amount)))
          </div>
        | `Bitcoin({bitcoinAmount, bitcoinUri, receiverAddress}) =>
          <div>
            <a href=bitcoinUri> (ReasonReact.stringToElement(bitcoinUri)) </a>
            <h4> (ReasonReact.stringToElement("Receiver Address")) </h4>
            (ReasonReact.stringToElement(receiverAddress))
            <h4> (ReasonReact.stringToElement("Amount")) </h4>
            (ReasonReact.stringToElement(string_of_float(bitcoinAmount) ++ " BTC"))
          </div>
        }
      )
      Routing.linkHome
    </div>
};
