let component = ReasonReact.statelessComponent("Redirect");

let make = (~clientSecret, ~sourceId, ~stripe, _children) => {
  ...component,
  render: (_self) =>
    <div>
      <Stripe.RetrieveSource
        stripe
        clientSecret
        sourceId
        render=(
          (result) =>
            switch result {
            | Stripe.RetrieveSource.Loading => ReasonReact.stringToElement("fetching source")
            | Stripe.RetrieveSource.Loaded(source) =>
              switch (Stripe.Source.getAmount(source), Stripe.Source.getCurrency(source)) {
              | (Some(amount), Some(currency)) =>
                <Donation.MakeStripeDonationMutation>
                  (
                    (makeMutation, mutationResult) =>
                      switch mutationResult {
                      | Donation.MakeStripeDonationMutation.NotCalled =>
                        makeMutation(
                          ~variables={
                            "input": {
                              "paymentMethod": Stripe.Source.getType(source),
                              "amount": {"amount": amount, "currency": currency},
                              "stripeSourceId": Stripe.Source.getId(source)
                            }
                          },
                          ()
                        );
                        ReasonReact.stringToElement("starting donation")
                      | Donation.MakeStripeDonationMutation.Loading =>
                        ReasonReact.stringToElement("making donation")
                      | Donation.MakeStripeDonationMutation.Loaded(response) =>
                        <Routing.Redirect
                          route=(
                            Routing.Routes.Pending(response##makeDonation##donation##id, `Simple)
                          )
                        />
                      | Donation.MakeStripeDonationMutation.Failed(error) =>
                        <div>
                          (ReasonReact.stringToElement("failed making mutation:"))
                          <pre> (ReasonReact.stringToElement(error)) </pre>
                          Routing.linkHome
                        </div>
                      }
                  )
                </Donation.MakeStripeDonationMutation>
              | _ => <div> (ReasonReact.stringToElement("invalid source")) Routing.linkHome </div>
              }
            | Stripe.RetrieveSource.Failed(error) =>
              <div>
                (ReasonReact.stringToElement("failed fetching source:"))
                <pre> (ReasonReact.stringToElement(error##message)) </pre>
                Routing.linkHome
              </div>
            }
        )
      />
    </div>
};
