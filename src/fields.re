module ElementTextField = {
  module WrapStripeElementForTextField = {
    let component = ReasonReact.statelessComponent("WrapStripeElementForTextField");
    let make = (~onChange, ~className=?, ~render, _children) => {
      ...component,
      render: (_) =>
        <div style=(ReactDOMRe.Style.make(~width="100%", ())) ?className>
          (
            render(
              ~onChange=
                (value: Stripe.CardNumberElement.onChangeValue) =>
                  (
                    switch (
                      Js.to_bool(value##empty),
                      Js.to_bool(value##complete),
                      Js.toOption(value##error)
                    ) {
                    | (_, _, Some(error)) => {"target": {"value": error##message}}
                    | (true, _, _) => {"target": {"value": ""}}
                    | (_, true, _) => {"target": {"value": "complete"}}
                    | _ => {"target": {"value": "value"}}
                    }
                  )
                  |> onChange,
              ~placeholder=""
            )
          )
        </div>
    };
    let js = (render) =>
      ReasonReact.wrapReasonForJs(
        ~component,
        (props) =>
          make(~render, ~className=props##className, ~onChange=props##onChange, props##children)
      );
  };
  module MakeElementTextField = {
    let component = ReasonReact.statelessComponent("MakeElementTextField");
    let make = (~render, ~name, ~id=name, _children) => {
      ...component,
      render: (_) =>
        MaterialUI.(
          <FormControl fullWidth=true>
            <InputLabel htmlFor=id /> /* (ReasonReact.stringToElement(name)) </InputLabel> */
            <MaterialUi.Input
              key="cardInput"
              id
              name
              inputComponent=(WrapStripeElementForTextField.js(render))
            />
          </FormControl>
        )
    };
  };
  type element =
    | Card
    | CardNumber
    | CardExpiry
    | CardCVC
    | PostalCode;
  let component = ReasonReact.statelessComponent("Element");
  let make = (~element, _children) => {
    ...component,
    render: (_) =>
      switch element {
      | Card =>
        <MakeElementTextField
          name="card"
          render=((~onChange, ~placeholder) => <Stripe.CardElement onChange placeholder />)
        />
      | CardNumber =>
        <MakeElementTextField
          name="cardNumber"
          render=((~onChange, ~placeholder) => <Stripe.CardNumberElement onChange placeholder />)
        />
      | CardExpiry =>
        <MakeElementTextField
          name="cardExpiry"
          render=((~onChange, ~placeholder) => <Stripe.CardExpiryElement onChange placeholder />)
        />
      | CardCVC =>
        <MakeElementTextField
          name="cardCVC"
          render=((~onChange, ~placeholder) => <Stripe.CardCVCElement onChange placeholder />)
        />
      | PostalCode =>
        <MakeElementTextField
          name="postalCode"
          render=((~onChange, ~placeholder) => <Stripe.PostalCodeElement onChange placeholder />)
        />
      }
  };
};
