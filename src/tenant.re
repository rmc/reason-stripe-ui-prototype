[@bs.module] external gql : ReasonApolloTypes.gql = "graphql-tag";

module TenantQueryConfig = {
  type fundraisingStripe = {. "connectedAccountId": string, "publishableKey": string};
  type fundraising = {. "stripe": fundraisingStripe};
  type response = {. "tenant": {. "fundraising": fundraising}};
  type variables;
  let query =
    [@bs]
    gql(
      {|
      query TenantQuery{
        tenant{
          fundraising{
            stripe{
              connectedAccountId
              publishableKey
            }
          }
        }
      }
    |}
    );
};

module TenantQuery = Apollo.Client.Query(TenantQueryConfig);
