module PaymentMethod = {
  type t =
    | Card
    | Alipay
    | P24
    | ACHCreditTransfer
    | Bancontact
    | Bitcoin
    | Giropay
    | Ideal
    | Sepa
    | Sofort
    | PaymentRequest;
  let toString = (paymentMethod) =>
    switch paymentMethod {
    | Card => "card"
    | Alipay => "alipay"
    | P24 => "p24"
    | ACHCreditTransfer => "ach_credit_transfer"
    | Bancontact => "bancontact"
    | Bitcoin => "bitcoin"
    | Giropay => "giropay"
    | Ideal => "ideal"
    | Sepa => "sepa_debit"
    | Sofort => "sofort"
    | PaymentRequest => "payment_request"
    };
  let ofString = (value) =>
    switch value {
    | "card" => Some(Card)
    | "p24" => Some(P24)
    | "alipay" => Some(Alipay)
    | "ach_credit_transfer" => Some(ACHCreditTransfer)
    | "bancontact" => Some(Bancontact)
    | "bitcoin" => Some(Bitcoin)
    | "giropay" => Some(Giropay)
    | "ideal" => Some(Ideal)
    | "sepa_debit" => Some(Sepa)
    | "sofort" => Some(Sofort)
    | "payment_request" => Some(PaymentRequest)
    | _ => None
    };
  let all = [
    Card,
    Alipay,
    P24,
    ACHCreditTransfer,
    Bancontact,
    Bitcoin,
    Giropay,
    Ideal,
    Sepa,
    Sofort,
    PaymentRequest
  ];
};

module Currency = {
  /* this module could be a functor that would get configured by the coutry */
  type t =
    | EUR
    | GBP
    | USD
    | PLN;
  let toString = (currency) =>
    switch currency {
    | EUR => "eur"
    | GBP => "gbp"
    | USD => "usd"
    | PLN => "pln"
    };
  let defaultCurrencyForCountry = EUR;
  let allCurrencies = [EUR, GBP, USD, PLN];
  let supportedCurrenciesForPaymentMethod = (paymentMethod) =>
    PaymentMethod.(
      switch paymentMethod {
      | Card => `Multiple(allCurrencies)
      | PaymentRequest
      | Alipay => `Single(defaultCurrencyForCountry)
      | P24 => `Multiple([EUR, PLN])
      | Bancontact
      | Ideal
      | Giropay
      | Sepa
      | Sofort => `Single(EUR)
      | ACHCreditTransfer
      | Bitcoin => `Single(USD)
      }
    );
};

[@bs.module] external gql : ReasonApolloTypes.gql = "graphql-tag";

module DonationQueryConfig = {
  type response = {
    .
    "donation": {. "id": string, "rejectionReason": Js.null(string), "state": string}
  };
  type variables = {. "id": string};
  let query =
    [@bs]
    gql(
      {|
      query DonationQuery($id: ID!){
        donation(id: $id){
          id
          rejectionReason
          state
        }
      }
    |}
    );
};

module DonationQuery = Apollo.Client.Query(DonationQueryConfig);

module DonationSubscriptionConfig = {
  type response = {
    .
    "donationUpdated": {
      .
      "donation": {. "id": string, "rejectionReason": Js.null(string), "state": string}
    }
  };
  type variables = {. "id": string};
  let subscription =
    [@bs]
    gql(
      {|
      subscription DonationUpdated($id: ID!){
        donationUpdated(id: $id){
          donation{
            id
            rejectionReason
            state
          }
        }
      }
    |}
    );
};

module DonationSubscription = Apollo.Client.Subscription(DonationSubscriptionConfig);

module MakeStripeDonationMutationConfig = {
  let mutation =
    [@bs]
    gql(
      {|
     mutation MakeStripeDonation($input: makeDonationInput!) {
         makeDonation(input: $input) {
           donation{
             id
             rejectionReason
             state
           }
         }
       }
|}
    );
  type response = {
    .
    "makeDonation": {
      .
      "donation": {. "id": string, "rejectionReason": Js.null(string), "state": string}
    }
  };
  type variables = {
    .
    "input": {
      .
      "paymentMethod": string,
      "amount": {. "amount": int, "currency": string},
      "stripeSourceId": string
    }
  };
  let makeVariables = (~paymentMethod, ~amount, ~currency, ~stripeSourceId) : variables => {
    "input": {
      "paymentMethod": paymentMethod,
      "amount": {"amount": amount, "currency": currency},
      "stripeSourceId": stripeSourceId
    }
  };
};

module MakeStripeDonationMutation = Apollo.Client.Mutation(MakeStripeDonationMutationConfig);

module MakeStripeDonationMutationFunc =
  Apollo.Client.MutationFunc(MakeStripeDonationMutationConfig);
