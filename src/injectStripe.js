import { injectStripe } from 'react-stripe-elements'

const InjectStripe = function InjectStripe(props) {
  return props.render(props)
}

export default injectStripe(InjectStripe)
