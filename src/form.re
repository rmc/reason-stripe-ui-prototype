module Money = {
  let component = ReasonReact.statelessComponent("Money");
  let getLabelForCurrency = (currency) =>
    Donation.Currency.(
      switch currency {
      | EUR => {js|Euro (€)|js}
      | USD => {js|American Dollar ($)|js}
      | GBP => {js|British Pound (£)|js}
      | PLN => {js|Polish Zloty (zł)|js}
      }
    );
  let make =
      (
        ~currencies: [ | `Multiple(list(Donation.Currency.t)) | `Single(Donation.Currency.t)],
        _children
      ) => {
    ...component,
    render: (_) =>
      <MaterialUI.FormGroup>
        <MaterialUI.InputLabel> (ReasonReact.stringToElement("Amount")) </MaterialUI.InputLabel>
        <FinalForm.Field
          name="amount"
          render=(
            (field) =>
              <MaterialUI.TextField
                _type="number"
                autoComplete="amount"
                onChange=(FinalForm.Field.FieldRenderProps.onChange(field))
                value=(`Int(FinalForm.Field.FieldRenderProps.value(field)))
                fullWidth=true
              />
          )
        />
        <FinalForm.Field
          name="currency"
          render=(
            (field) =>
              <MaterialUI.Select
                placeholder="currency"
                onChange=(FinalForm.Field.FieldRenderProps.onChange(field))
                value=(`String(FinalForm.Field.FieldRenderProps.value(field)))
                fullWidth=true
                disabled=(
                  switch currencies {
                  | `Single(_) => true
                  | `Multiple(_) => false
                  }
                )>
                (
                  switch currencies {
                  | `Single(currency) =>
                    <MaterialUI.MenuItem value=(`String(Donation.Currency.toString(currency)))>
                      (ReasonReact.stringToElement(getLabelForCurrency(currency)))
                    </MaterialUI.MenuItem>
                  | `Multiple(currencyList) =>
                    currencyList
                    |> List.map(
                         (currency) =>
                           <MaterialUI.MenuItem
                             value=(`String(Donation.Currency.toString(currency)))>
                             (ReasonReact.stringToElement(getLabelForCurrency(currency)))
                           </MaterialUI.MenuItem>
                       )
                    |> Array.of_list
                    |> ReasonReact.arrayToElement
                  }
                )
              </MaterialUI.Select>
          )
        />
      </MaterialUI.FormGroup>
  };
};

let returnUrl =
  switch [%external window] {
  | None => raise @@ Invalid_argument("failed creating return url: window object is not defined")
  | Some((window: Dom.window)) => (window |> Routing.location |> Routing.origin) ++ "/redirect"
  };

let defaultCurrencyForPaymentMethod = (pm) =>
  switch (Donation.Currency.supportedCurrenciesForPaymentMethod(pm)) {
  | `Multiple(currencies) => List.hd(currencies)
  | `Single(currency) => currency
  };

let emailField =
  <FinalForm.Field
    name="email"
    render=(
      (field) =>
        <MaterialUI.TextField
          placeholder="email"
          autoComplete="email"
          onChange=(FinalForm.Field.FieldRenderProps.onChange(field))
          value=(`String(FinalForm.Field.FieldRenderProps.value(field)))
          fullWidth=true
        />
    )
  />;

let nameField =
  <FinalForm.Field
    name="name"
    render=(
      (field) =>
        <MaterialUI.TextField
          placeholder="name"
          autoComplete="name"
          onChange=(FinalForm.Field.FieldRenderProps.onChange(field))
          value=(`String(FinalForm.Field.FieldRenderProps.value(field)))
          fullWidth=true
        />
    )
  />;

let renderFieldsForPaymentMethod = (pm) =>
  <React.Fragment>
    <Money currencies=(Donation.Currency.supportedCurrenciesForPaymentMethod(pm)) />
    Donation.PaymentMethod.(
      switch pm {
      | Card => <Fields.ElementTextField element=Fields.ElementTextField.Card />
      | P24 => emailField
      | Alipay => ReasonReact.nullElement
      | ACHCreditTransfer => emailField
      | Bancontact => nameField
      | Bitcoin => emailField
      | Giropay => nameField
      | Ideal => ReasonReact.nullElement
      | Sepa =>
        <React.Fragment>
          nameField
          <FinalForm.Field
            name="iban"
            render=(
              (field) =>
                <MaterialUI.TextField
                  placeholder="iban"
                  autoComplete="iban"
                  onChange=(FinalForm.Field.FieldRenderProps.onChange(field))
                  value=(`String(FinalForm.Field.FieldRenderProps.value(field)))
                  fullWidth=true
                />
            )
          />
          <MaterialUI.Typography component="p" _type="body2">
            (
              "By providing your IBAN and confirming this payment, you are authorizing Civist and Stripe, our payment service provider, to send instructions to your bank to debit your account and your bank to debit your account in accordance with those instructions. You are entitled to a refund from your bank under the terms and conditions of your agreement with your bank. A refund must be claimed within 8 weeks starting from the date on which your account was debited."
              |> ReasonReact.stringToElement
            )
          </MaterialUI.Typography>
        </React.Fragment>
      | Sofort =>
        <FinalForm.Field
          name="country"
          render=(
            (field) =>
              <React.Fragment>
                nameField
                <MaterialUI.InputLabel htmlFor="country">
                  (ReasonReact.stringToElement("Country"))
                </MaterialUI.InputLabel>
                <MaterialUI.Select
                  placeholder="country"
                  onChange=(FinalForm.Field.FieldRenderProps.onChange(field))
                  value=(`String(FinalForm.Field.FieldRenderProps.value(field)))
                  fullWidth=true>
                  (
                    ["AU", "BE", "DE", "IT", "NL", "ES"]
                    |> List.map(
                         (country) =>
                           <MaterialUI.MenuItem value=(`String(country))>
                             (ReasonReact.stringToElement(country))
                           </MaterialUI.MenuItem>
                       )
                    |> Array.of_list
                    |> ReasonReact.arrayToElement
                  )
                </MaterialUI.Select>
              </React.Fragment>
          )
        />
      | PaymentRequest => ReasonReact.nullElement
      }
    )
  </React.Fragment>;

let createCardSource = (stripe, values) =>
  Stripe.CreateCardSource.create(
    ~variables=Stripe.CardSource.makeVariables(~amount=values##amount, ~currency=values##currency),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`Card(source)));

let createAlipaySource = (stripe, values) =>
  Stripe.CreateAlipaySource.create(
    ~variables=
      Stripe.AlipaySource.makeVariables(
        ~amount=values##amount,
        ~currency=values##currency,
        ~returnUrl
      ),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`Alipay(source)));

let createBancontactSource = (stripe, values) =>
  Stripe.CreateBancontactSource.create(
    ~variables=
      Stripe.BancontactSource.makeVariables(
        ~amount=values##amount,
        ~currency=values##currency,
        ~name=values##name,
        ~returnUrl
      ),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`Bancontact(source)));

let createIdealSource = (stripe, values) =>
  Stripe.CreateIdealSource.create(
    ~variables=
      Stripe.IdealSource.makeVariables(
        ~amount=values##amount,
        ~currency=values##currency,
        ~returnUrl
      ),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`Ideal(source)));

let createGiropaySource = (stripe, values) =>
  Stripe.CreateGiropaySource.create(
    ~variables=
      Stripe.GiropaySource.makeVariables(
        ~name=values##name,
        ~amount=values##amount,
        ~currency=values##currency,
        ~returnUrl
      ),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`Giropay(source)));

let createP24Source = (stripe, values) =>
  Stripe.CreateP24Source.create(
    ~variables=
      Stripe.P24Source.makeVariables(
        ~amount=values##amount,
        ~currency=values##currency,
        ~email=values##email,
        ~returnUrl
      ),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`P24(source)));

let createSofortSource = (stripe, values) =>
  Stripe.CreateSofortSource.create(
    ~variables=
      Stripe.SofortSource.makeVariables(
        ~amount=values##amount,
        ~currency=values##currency,
        ~name=values##name,
        ~country=values##country,
        ~returnUrl
      ),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`Sofort(source)));

let createACHCreditTransferSource = (stripe, values) =>
  Stripe.CreateACHCreditTransferSource.create(
    ~variables=
      Stripe.ACHCreditTransferSource.makeVariables(
        ~amount=values##amount,
        ~currency=values##currency,
        ~email=values##email
      ),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`ACHCreditTransfer(source)));

let createBitcoinSource = (stripe, values) =>
  Stripe.CreateBitcoinSource.create(
    ~variables=
      Stripe.BitcoinSource.makeVariables(
        ~amount=values##amount,
        ~currency=values##currency,
        ~email=values##email
      ),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`Bitcoin(source)));

let createSepaSource = (stripe, values) =>
  Stripe.CreateSepaSource.create(
    ~variables=
      Stripe.SepaSource.makeVariables(
        ~amount=values##amount,
        ~currency=values##currency,
        ~name=values##name,
        ~iban=values##iban
      ),
    stripe
  )
  |> Js.Promise.then_((source) => Js.Promise.resolve(`Sepa(source)));

let handleSourceCreated = (~stripe, source) =>
  switch source {
  | `Alipay(({redirectUrl}: Stripe.AlipaySource.t))
  | `Bancontact(({redirectUrl}: Stripe.BancontactSource.t))
  | `Ideal(({redirectUrl}: Stripe.IdealSource.t))
  | `Giropay(({redirectUrl}: Stripe.GiropaySource.t))
  | `P24(({redirectUrl}: Stripe.P24Source.t))
  | `Sofort(({redirectUrl}: Stripe.SofortSource.t)) =>
    Js.Promise.resolve(`SourceNeedsVerification(redirectUrl))
  | `ACHCreditTransfer((source: Stripe.ACHCreditTransferSource.t)) =>
    Js.Promise.resolve(`SourceNeedsUserAction(`ACHCreditTransfer(source)))
  | `Bitcoin((source: Stripe.BitcoinSource.t)) =>
    Js.Promise.resolve(`SourceNeedsUserAction(`Bitcoin(source)))
  | `Sepa(({id: sourceId, amount, currency}: Stripe.SepaSource.t)) =>
    Js.Promise.resolve(`SourceChargeable(("sepa_debit", sourceId, amount, currency)))
  | `Card(
      (
        {id: sourceId, amount, currency, threeDSecure: Stripe.CardSource.ThreeDSecure.NotAvailable}: Stripe.CardSource.t
      )
    ) =>
    Js.Promise.resolve(`SourceChargeable(("card", sourceId, amount, currency)))
  | `Card(
      (
        {
          id: sourceId,
          amount,
          currency,
          threeDSecure:
            Stripe.CardSource.ThreeDSecure.Required | Stripe.CardSource.ThreeDSecure.Optional
        }: Stripe.CardSource.t
      )
    ) =>
    Stripe.CreateThreeDSecureSource.create(
      ~variables=
        Stripe.ThreeDSecureSource.makeVariables(~amount, ~currency, ~cardId=sourceId, ~returnUrl),
      stripe
    )
    |> Js.Promise.then_(
         ({id: sourceId}: Stripe.ThreeDSecureSource.t) =>
           Js.Promise.resolve(`SourceChargeable(("three_d_secure", sourceId, amount, currency)))
       )
  };

let labelForPaymentMethod = (pm) =>
  Donation.PaymentMethod.(
    switch pm {
    | Card => "Card"
    | Alipay => "Alipay"
    | P24 => "P24"
    | ACHCreditTransfer => "ACH Credit Transfer"
    | Bancontact => "Bancontact"
    | Bitcoin => "Bitcoin"
    | Giropay => "Giropay"
    | Ideal => "Ideal"
    | Sepa => "Sepa"
    | Sofort => "Sofort"
    | PaymentRequest => "PaymentRequest"
    }
  );

/* let email = "test+fill_now@example.com";

   let name = "succeeding_charge";

   let ibanSucceed = "DE89370400440532013000";

   let ibanFail = "DE62370400440532013001";

   let country = "DE"; */
let onSubmit = (stripe, values, _) =>
  Js.Promise.(
    Donation.PaymentMethod.(
      switch values##paymentMethod {
      | PaymentRequest => resolve(`NoOp)
      | Card =>
        createCardSource(stripe, values) |> then_((source) => handleSourceCreated(~stripe, source))
      | Alipay =>
        createAlipaySource(stripe, values)
        |> then_((source) => handleSourceCreated(~stripe, source))
      | P24 =>
        createP24Source(stripe, values) |> then_((source) => handleSourceCreated(~stripe, source))
      | Bancontact =>
        createBancontactSource(stripe, values)
        |> then_((source) => handleSourceCreated(~stripe, source))
      | Giropay =>
        createGiropaySource(stripe, values)
        |> then_((source) => handleSourceCreated(~stripe, source))
      | Ideal =>
        createIdealSource(stripe, values)
        |> then_((source) => handleSourceCreated(~stripe, source))
      | Sofort =>
        createSofortSource(stripe, values)
        |> then_((source) => handleSourceCreated(~stripe, source))
      | ACHCreditTransfer =>
        createACHCreditTransferSource(stripe, values)
        |> then_((source) => handleSourceCreated(~stripe, source))
      | Bitcoin =>
        createBitcoinSource(stripe, values)
        |> then_((source) => handleSourceCreated(~stripe, source))
      | Sepa =>
        createSepaSource(stripe, values) |> then_((source) => handleSourceCreated(~stripe, source))
      }
    )
    |> then_(
         (result) => {
           switch result {
           | `SourceChargeable(pm, sourceId, amount, currency) =>
             Donation.MakeStripeDonationMutationFunc.make(
               ~variables=
                 Donation.MakeStripeDonationMutationConfig.makeVariables(
                   ~paymentMethod=pm,
                   ~amount,
                   ~currency,
                   ~stripeSourceId=sourceId
                 ),
               ()
             )
             |> then_(
                  (donation) => {
                    let donationId = donation##makeDonation##donation##id;
                    ReasonReact.Router.push(
                      Routing.Routes.(toString(Pending(donationId, `Simple)))
                    );
                    resolve()
                  }
                )
             |> ignore
           | `SourceNeedsUserAction(
               `Bitcoin(
                 (
                   {id, amount, currency, receiverAddress, bitcoinAmount, bitcoinUri}: Stripe.BitcoinSource.t
                 )
               )
             ) =>
             Donation.MakeStripeDonationMutationFunc.make(
               ~variables=
                 Donation.MakeStripeDonationMutationConfig.makeVariables(
                   ~paymentMethod="bitcoin",
                   ~amount,
                   ~currency,
                   ~stripeSourceId=id
                 ),
               ()
             )
             |> then_(
                  (response) => {
                    let donationId = response##makeDonation##donation##id;
                    Routing.Routes.(
                      Pending(donationId, `Bitcoin({receiverAddress, bitcoinUri, bitcoinAmount}))
                      |> toString
                    )
                    |> ReasonReact.Router.push;
                    resolve()
                  }
                )
             |> ignore
           | `SourceNeedsUserAction(
               `ACHCreditTransfer(
                 (
                   {id, amount, currency, routingNumber, accountNumber}: Stripe.ACHCreditTransferSource.t
                 )
               )
             ) =>
             Donation.MakeStripeDonationMutationFunc.make(
               ~variables=
                 Donation.MakeStripeDonationMutationConfig.makeVariables(
                   ~paymentMethod="ach_credit_transfer",
                   ~amount,
                   ~currency,
                   ~stripeSourceId=id
                 ),
               ()
             )
             |> then_(
                  (response) => {
                    let donationId = response##makeDonation##donation##id;
                    Routing.Routes.(
                      Pending(
                        donationId,
                        `ACHCreditTransfer({amount, routingNumber, accountNumber})
                      )
                      |> toString
                    )
                    |> ReasonReact.Router.push;
                    resolve()
                  }
                )
             |> ignore
           | `SourceNeedsVerification(redirectUrl) =>
             ReasonReact.Router.push(Routing.Routes.(toString(External(redirectUrl))))
           | `NoOp => ()
           };
           resolve(Js.Undefined.empty)
         }
       )
    |> catch(
         (error) => {
           let handleTypedRejections =
             [@bs.open]
             (
               fun
               | Stripe.StripeResultError(stripeError) =>
                 FinalForm.FormErrors.make(~submissionError=stripeError##message, ())
             );
           handleTypedRejections(error)
           |> Utils.defaultOpt(
                FinalForm.FormErrors.make(~submissionError="Something went wrong", ())
              )
           |> resolve
         }
       )
  );

let component = ReasonReact.statelessComponent("DonationForm");

let make = (~paymentMethod, ~stripeAccountCountry, ~stripe, _children) => {
  ...component,
  render: (_self) =>
    <div>
      <FinalForm.Form
        initialValues={
          "paymentMethod": paymentMethod,
          "currency": Donation.Currency.toString(defaultCurrencyForPaymentMethod(paymentMethod)),
          "amount": 2500,
          "country":
            switch paymentMethod {
            | Donation.PaymentMethod.Bitcoin => stripeAccountCountry
            | _ => "DE"
            }
        }
        onSubmit=(`AsyncPromise(onSubmit(stripe)))
        render=(
          (form) => {
            let (snackbarOpen, snackbarMessage) =
              switch (
                FinalForm.Form.FormRenderProps.submitError(form) |> Js.Undefined.to_opt,
                FinalForm.Form.FormRenderProps.dirtySinceLastSubmit(form) |> Js.to_bool
              ) {
              | (Some(error), false) => (true, error)
              | _ => (false, "")
              };
            let isLoading = FinalForm.Form.FormRenderProps.submitting(form) |> Js.to_bool;
            <React.Fragment>
              <MaterialUi.Tabs
                scrollable=true
                value=paymentMethod
                onChange=(
                  (_, newPaymentMethod) =>
                    newPaymentMethod |> Donation.PaymentMethod.toString |> ReasonReact.Router.push
                )>
                (
                  Donation.PaymentMethod.all
                  |> List.map(
                       (pm) =>
                         <MaterialUi.Tab
                           key=(pm |> Donation.PaymentMethod.toString)
                           value=pm
                           label=(labelForPaymentMethod(pm) |> ReasonReact.stringToElement)
                         />
                     )
                )
              </MaterialUi.Tabs>
              <div style=(ReactDOMRe.Style.make(~height="24px", ())) />
              <form onSubmit=(FinalForm.Form.FormRenderProps.handleSubmit(form))>
                <div style=(ReactDOMRe.Style.make(~width="100%", ~marginBottom="32px", ()))>
                  (renderFieldsForPaymentMethod(paymentMethod))
                </div>
                <div style=(ReactDOMRe.Style.make(~textAlign="center", ()))>
                  (
                    switch paymentMethod {
                    | PaymentRequest =>
                      let values = FinalForm.Form.FormRenderProps.values(form);
                      <Stripe.PaymentRequest
                        stripe
                        options={
                          "country": values##country,
                          "currency": values##currency,
                          "total": {
                            "amount": values##amount,
                            "label": "test",
                            "pending": Js.Undefined.empty
                          }
                        }
                        render=(
                          (state) =>
                            switch state {
                            | Stripe.PaymentRequest.CanMakePayment(paymentRequest) =>
                              <Stripe.PaymentRequestButtonElement paymentRequest />
                            | Stripe.PaymentRequest.Loading =>
                              "Payment Request Loading" |> ReasonReact.stringToElement
                            | Stripe.PaymentRequest.Failed =>
                              "Payment Request not available" |> ReasonReact.stringToElement
                            | Stripe.PaymentRequest.Loaded(result) =>
                              let complete = result##complete;
                              let source = result##source;
                              Donation.MakeStripeDonationMutationFunc.make(
                                ~variables=
                                  Donation.MakeStripeDonationMutationConfig.makeVariables(
                                    ~paymentMethod=source##_type,
                                    ~amount=values##amount,
                                    ~currency=values##currency,
                                    ~stripeSourceId=source##id
                                  ),
                                ()
                              )
                              |> Js.Promise.then_(
                                   (donation) => {
                                     complete("success");
                                     let donationId = donation##makeDonation##donation##id;
                                     ReasonReact.Router.push(
                                       Routing.Routes.(toString(Pending(donationId, `Simple)))
                                     );
                                     Js.Promise.resolve()
                                   }
                                 )
                              |> Js.Promise.catch(
                                   (_error) => {
                                     complete("fail");
                                     Js.Promise.resolve()
                                   }
                                 )
                              |> ignore;
                              "making donation" |> ReasonReact.stringToElement
                            }
                        )
                      />
                    | _ =>
                      <MaterialUi.Button type_="submit" raised=true disabled=isLoading>
                        (ReasonReact.stringToElement("donate"))
                      </MaterialUi.Button>
                    }
                  )
                </div>
              </form>
              <MaterialUi.Snackbar
                open_=snackbarOpen
                message=snackbarMessage
                onClose=((_, _) => ())
              />
            </React.Fragment>
          }
        )
      />
    </div>
};
