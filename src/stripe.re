module Provider = {
  [@bs.module "react-stripe-elements"] external provider : ReasonReact.reactClass =
    "StripeProvider";
  let make = (~apiKey: string, ~stripeAccount: option(string)=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass=provider,
      ~props={"apiKey": apiKey, "stripeAccount": stripeAccount |> Js.Undefined.from_opt},
      children
    );
};

module Elements = {
  [@bs.module "react-stripe-elements"] external elements : ReasonReact.reactClass = "Elements";
  let make = (children) =>
    ReasonReact.wrapJsForReason(~reactClass=elements, ~props=Js.Obj.empty(), children);
};

module Stripe = {
  type t;
  module Source = {
    type stripe = t;
    type result('source, 'error) = {
      .
      "error": Js.nullable('error), "source": Js.nullable('source)
    };
    [@bs.send] external create' : (stripe, 'options) => Js.Promise.t(result('source, 'error)) =
      "createSource";
    let create = (~options=?, stripe) => create'(stripe, Js.Undefined.from_opt(options));
    [@bs.send]
    external retrieve' :
      (t, {. "id": string, "client_secret": string}) => Js.Promise.t(result('source, 'error)) =
      "retrieveSource";
    let retrieve = (~options, stripe) => retrieve'(stripe, options);
  };
  module PaymentRequest = {
    type stripe = t;
    type t;
    type variables = {
      .
      "country": string,
      "currency": string,
      "total": {. "amount": int, "label": string, "pending": Js.Undefined.t(Js.boolean)}
    };
    type paymentResponseCompleteFunction = string => unit; /* TODO: constraint input with a variant instead of string */
    type sourcePaymentResponse('source) = {
      .
      "source": 'source, "complete": paymentResponseCompleteFunction, "methodName": string
    };
    [@bs.send] external paymentRequest' : (stripe, variables) => t = "paymentRequest";
    let make = (~options, stripe) => paymentRequest'(stripe, options);
    [@bs.send] external canMakePayment : t => Js.Promise.t(Js.Null.t('a)) = "";
    [@bs.send]
    external onSource :
      (t, [@bs.as "source"] _, sourcePaymentResponse('source) => unit) =>
      Js.Promise.t(Js.Null.t('a)) =
      "on";
  };
};

module InjectStripe = {
  [@bs.module "./injectStripe"] external injectStripe : ReasonReact.reactClass = "default";
  let make = (~render: {.. "stripe": Stripe.t} => ReasonReact.reactElement, children) =>
    ReasonReact.wrapJsForReason(~reactClass=injectStripe, ~props={"render": render}, children);
};

module ResultError = {
  type t = {. "message": string};
};

module Source = {
  type t = {
    .
    "id": string,
    "_type": string,
    "amount": Js.Nullable.t(int),
    "currency": Js.Nullable.t(string),
    "card": Js.Nullable.t(card),
    "redirect": Js.Nullable.t(redirect)
  }
  and redirect = {. "url": string}
  and card = {. "id": string, "three_d_secure": string};
  let getId = (source: t) => source##id;
  let getType = (source: t) => source##_type;
  let getAmount = (source: t) => source##amount |> Js.Nullable.to_opt;
  let getCurrency = (source: t) => source##currency |> Js.Nullable.to_opt;
  let getRedirect = (source: t) =>
    switch (Js.Nullable.to_opt(source##redirect)) {
    | Some(redirect) => Some(redirect)
    | None => None
    };
};

exception StripeResultError(ResultError.t);

exception UnhandledResultType;

module RetrieveSource = {
  type action =
    | Result(Source.t)
    | Error(ResultError.t);
  type state =
    | Loading
    | Loaded(Source.t)
    | Failed(ResultError.t);
  let component = ReasonReact.reducerComponent("RetrieveSource");
  let make = (~render, ~sourceId, ~clientSecret, ~stripe, _children) => {
    ...component,
    initialState: () => Loading,
    didMount: ({reduce}) => {
      Stripe.Source.retrieve(stripe, ~options={"id": sourceId, "client_secret": clientSecret})
      |> Js.Promise.then_(
           (result) => {
             let error = result##error |> Js.Nullable.to_opt;
             let source = result##source |> Js.Nullable.to_opt;
             switch (error, source) {
             | (Some(error), _) => reduce((_) => Error(error), ())
             | (_, Some(source)) => reduce((_) => Result(source), ())
             | _ => raise @@ UnhandledResultType
             };
             Js.Promise.resolve()
           }
         )
      |> ignore;
      ReasonReact.NoUpdate
    },
    reducer: (action: action, _: state) =>
      switch action {
      | Result(source) => ReasonReact.Update(Loaded(source))
      | Error(error) => ReasonReact.Update(Failed(error))
      },
    render: ({state}) => render(state)
  };
};

module type CreateSourceConfig = {type variables; type result; type t; let cast: result => t;};

module CreateSourceFactory = (Config: CreateSourceConfig) => {
  let create = (~variables: Config.variables, stripe: Stripe.t) =>
    Stripe.Source.create(~options=variables, stripe)
    |> Js.Promise.then_(
         (result) => {
           let error = result##error |> Js.Nullable.to_opt;
           let source = result##source |> Js.Nullable.to_opt;
           switch (error, source) {
           | (Some(error), _) => Js.Promise.reject(StripeResultError(error))
           | (_, Some(source)) => Js.Promise.resolve(Config.cast(source))
           | _ => Js.Promise.reject(UnhandledResultType)
           }
         }
       );
};

module CreateSourceReactComponentFactory = (Config: CreateSourceConfig) => {
  type action =
    | Create(Stripe.t, Config.variables)
    | Result(Config.result)
    | Error(ResultError.t)
    | Reset
  and state =
    | NotCalled
    | Loading
    | Loaded(Config.t)
    | Failed(ResultError.t);
  let component = ReasonReact.reducerComponent("CreateSource");
  let make = (~stripe, ~render, _children) => {
    ...component,
    initialState: () => NotCalled,
    reducer: (action, _state) =>
      switch action {
      | Create(stripe, (variables: Config.variables)) =>
        ReasonReact.UpdateWithSideEffects(
          Loading,
          (
            ({send}) =>
              Stripe.Source.create(~options=variables, stripe)
              |> Js.Promise.then_(
                   (result) => {
                     let error = result##error |> Js.Nullable.to_opt;
                     let source = result##source |> Js.Nullable.to_opt;
                     switch (error, source) {
                     | (Some(error), _) => send(Error(error))
                     | (_, Some(source)) => send(Result(source))
                     | _ => raise @@ UnhandledResultType
                     };
                     Js.Promise.resolve()
                   }
                 )
              |> ignore
          )
        )
      | Result(source) => ReasonReact.Update(Loaded(source |> Config.cast))
      | Error(error) => ReasonReact.Update(Failed(error))
      | Reset => ReasonReact.Update(NotCalled)
      },
    render: ({state, send}) =>
      render(state, (variables) => send(Create(stripe, variables)), () => send(Reset))
  };
};

/* Sources */
module CardSource = {
  type variables = {. "amount": int, "currency": string};
  type result = {. "id": string, "amount": int, "currency": string, "card": card}
  and redirect = {. "url": string}
  and card = {. "id": string, "three_d_secure": string};
  let makeVariables = (~amount, ~currency) => {"amount": amount, "currency": currency};
  module ThreeDSecure = {
    type t =
      | Required
      | Optional
      | NotAvailable;
    let getThreeDSecure = (source) =>
      switch source##card##three_d_secure {
      | "required" => Required
      | "optional" => Optional
      | "not_available"
      | _ => NotAvailable
      };
  };
  type t = {
    id: string,
    amount: int,
    currency: string,
    threeDSecure: ThreeDSecure.t
  };
  let cast = (result: result) => {
    id: result##id,
    amount: result##amount,
    currency: result##currency,
    threeDSecure: ThreeDSecure.getThreeDSecure(result)
  };
};

module CreateCardSourceComponent = CreateSourceReactComponentFactory(CardSource);

module CreateCardSource = CreateSourceFactory(CardSource);

module ThreeDSecureSource = {
  type variables = {
    .
    "type": string,
    "amount": int,
    "currency": string,
    "three_d_secure": {. "card": string},
    "redirect": {. "return_url": string}
  };
  type result = {. "id": string, "amount": int, "currency": string, "redirect": {. "url": string}};
  type t = {
    id: string,
    amount: int,
    currency: string,
    redirectUrl: string
  };
  let makeVariables = (~amount, ~currency, ~cardId, ~returnUrl) => {
    "type": "three_d_secure",
    "amount": amount,
    "currency": currency,
    "three_d_secure": {"card": cardId},
    "redirect": {"return_url": returnUrl}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency,
    redirectUrl: source##redirect##url
  };
};

module CreateThreeDSecureSourceComponent = CreateSourceReactComponentFactory(ThreeDSecureSource);

module CreateThreeDSecureSource = CreateSourceFactory(ThreeDSecureSource);

module P24Source = {
  type variables = {
    .
    "type": string,
    "amount": int,
    "currency": string,
    "owner": {. "email": string},
    "redirect": {. "return_url": string}
  };
  type result = {. "id": string, "amount": int, "currency": string, "redirect": {. "url": string}};
  type t = {
    id: string,
    amount: int,
    currency: string,
    redirectUrl: string
  };
  let makeVariables = (~amount, ~currency, ~email, ~returnUrl) : variables => {
    "type": "p24",
    "amount": amount,
    "currency": currency,
    "owner": {"email": email},
    "redirect": {"return_url": returnUrl}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency,
    redirectUrl: source##redirect##url
  };
};

module CreateP24SourceComponent = CreateSourceReactComponentFactory(P24Source);

module CreateP24Source = CreateSourceFactory(P24Source);

module AlipaySource = {
  type variables = {
    .
    "type": string, "amount": int, "currency": string, "redirect": {. "return_url": string}
  };
  type result = {. "id": string, "amount": int, "currency": string, "redirect": {. "url": string}};
  type t = {
    id: string,
    amount: int,
    currency: string,
    redirectUrl: string
  };
  let makeVariables = (~amount, ~currency, ~returnUrl) : variables => {
    "type": "alipay",
    "amount": amount,
    "currency": currency,
    "redirect": {"return_url": returnUrl}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency,
    redirectUrl: source##redirect##url
  };
};

module CreateAlipaySourceComponent = CreateSourceReactComponentFactory(AlipaySource);

module CreateAlipaySource = CreateSourceFactory(AlipaySource);

module ACHCreditTransferSource = {
  type variables = {
    .
    "type": string, "amount": int, "currency": string, "owner": {. "email": string}
  };
  type result = {
    .
    "id": string,
    "amount": int,
    "currency": string,
    "ach_credit_transfer": {. "routing_number": string, "account_number": string}
  };
  type t = {
    id: string,
    amount: int,
    currency: string,
    routingNumber: string,
    accountNumber: string
  };
  let makeVariables = (~amount, ~currency, ~email) : variables => {
    "type": "ach_credit_transfer",
    "amount": amount,
    "currency": currency,
    "owner": {"email": email}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency,
    routingNumber: source##ach_credit_transfer##routing_number,
    accountNumber: source##ach_credit_transfer##account_number
  };
};

module CreateACHCreditTransferSourceComponent =
  CreateSourceReactComponentFactory(ACHCreditTransferSource);

module CreateACHCreditTransferSource = CreateSourceFactory(ACHCreditTransferSource);

module BancontactSource = {
  type variables = {
    .
    "type": string,
    "amount": int,
    "currency": string,
    "owner": {. "name": string},
    "redirect": {. "return_url": string}
  };
  type result = {. "id": string, "amount": int, "currency": string, "redirect": {. "url": string}};
  type t = {
    id: string,
    amount: int,
    currency: string,
    redirectUrl: string
  };
  let makeVariables = (~amount, ~currency, ~name, ~returnUrl) : variables => {
    "type": "bancontact",
    "amount": amount,
    "currency": currency,
    "owner": {"name": name},
    "redirect": {"return_url": returnUrl}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency,
    redirectUrl: source##redirect##url
  };
};

module CreateBancontactSourceComponent = CreateSourceReactComponentFactory(BancontactSource);

module CreateBancontactSource = CreateSourceFactory(BancontactSource);

module BitcoinSource = {
  type variables = {
    .
    "type": string, "amount": int, "currency": string, "owner": {. "email": string}
  };
  type result = {
    .
    "id": string,
    "amount": int,
    "currency": string,
    "receiver": {. "address": string},
    "bitcoin": {. "amount": int, "uri": string}
  };
  type t = {
    id: string,
    amount: int,
    currency: string,
    receiverAddress: string,
    bitcoinAmount: float,
    bitcoinUri: string
  };
  let makeVariables = (~amount, ~currency, ~email) : variables => {
    "type": "bitcoin",
    "amount": amount,
    "currency": currency,
    "owner": {"email": email}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency,
    receiverAddress: source##receiver##address,
    bitcoinAmount: float_of_int(source##bitcoin##amount) /. 100000000.0,
    bitcoinUri: source##bitcoin##uri
  };
};

module CreateBitcoinSourceComponent = CreateSourceReactComponentFactory(BitcoinSource);

module CreateBitcoinSource = CreateSourceFactory(BitcoinSource);

module GiropaySource = {
  type variables = {
    .
    "type": string,
    "amount": int,
    "currency": string,
    "owner": {. "name": string},
    "redirect": {. "return_url": string}
  };
  type result = {. "id": string, "amount": int, "currency": string, "redirect": {. "url": string}};
  type t = {
    id: string,
    amount: int,
    currency: string,
    redirectUrl: string
  };
  let makeVariables = (~amount, ~currency, ~name, ~returnUrl) : variables => {
    "type": "giropay",
    "amount": amount,
    "currency": currency,
    "owner": {"name": name},
    "redirect": {"return_url": returnUrl}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency,
    redirectUrl: source##redirect##url
  };
};

module CreateGiropaySourceComponent = CreateSourceReactComponentFactory(GiropaySource);

module CreateGiropaySource = CreateSourceFactory(GiropaySource);

module IdealSource = {
  type variables = {
    .
    "type": string, "amount": int, "currency": string, "redirect": {. "return_url": string}
  };
  type result = {. "id": string, "amount": int, "currency": string, "redirect": {. "url": string}};
  type t = {
    id: string,
    amount: int,
    currency: string,
    redirectUrl: string
  };
  let makeVariables = (~amount, ~currency, ~returnUrl) : variables => {
    "type": "ideal",
    "amount": amount,
    "currency": currency,
    "redirect": {"return_url": returnUrl}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency,
    redirectUrl: source##redirect##url
  };
};

module CreateIdealSourceComponent = CreateSourceReactComponentFactory(IdealSource);

module CreateIdealSource = CreateSourceFactory(IdealSource);

module SepaSource = {
  type variables = {
    .
    "type": string,
    "amount": int,
    "currency": string,
    "sepa_debit": {. "iban": string},
    "owner": {. "name": string}
  };
  type result = {. "id": string, "amount": int, "currency": string};
  type t = {
    id: string,
    amount: int,
    currency: string
  };
  let makeVariables = (~amount, ~currency, ~name, ~iban) : variables => {
    "type": "sepa_debit",
    "amount": amount,
    "currency": currency,
    "sepa_debit": {"iban": iban},
    "owner": {"name": name}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency
  };
};

module CreateSepaSourceComponent = CreateSourceReactComponentFactory(SepaSource);

module CreateSepaSource = CreateSourceFactory(SepaSource);

module SofortSource = {
  type variables = {
    .
    "type": string,
    "amount": int,
    "currency": string,
    "sofort": {. "country": string},
    "redirect": {. "return_url": string},
    "owner": {. "name": string}
  };
  type result = {. "id": string, "amount": int, "currency": string, "redirect": {. "url": string}};
  type t = {
    id: string,
    amount: int,
    currency: string,
    redirectUrl: string
  };
  let makeVariables = (~amount, ~currency, ~country, ~returnUrl, ~name) : variables => {
    "type": "sofort",
    "amount": amount,
    "currency": currency,
    "sofort": {"country": country},
    "redirect": {"return_url": returnUrl},
    "owner": {"name": name}
  };
  let cast = (source: result) => {
    id: source##id,
    amount: source##amount,
    currency: source##currency,
    redirectUrl: source##redirect##url
  };
};

module CreateSofortSourceComponent = CreateSourceReactComponentFactory(SofortSource);

module CreateSofortSource = CreateSourceFactory(SofortSource);

/* Elements  */
module StyleProp = {
  type t = {. "base": style}
  and style = {
    .
    "color": Js.undefined(string),
    "fontFamily": Js.undefined(string),
    "fontSize": Js.undefined(string),
    "fontWeight": Js.undefined(string)
  };
  [@bs.obj]
  external makeStyle :
    (~color: string=?, ~fontFamily: string=?, ~fontSize: string=?, ~fontWeight: string=?, unit) =>
    style =
    "";
};

module MakeElement = (Config: {let element: ReasonReact.reactClass;}) => {
  type onChangeValue = {
    .
    "empty": Js.boolean, "complete": Js.boolean, "error": Js.nullable(onChangeValueError)
  }
  and onChangeValueError = {. "message": string};
  let make =
      (
        ~onChange=?,
        ~onBlur=?,
        ~onFocus=?,
        ~placeholder: option(string)=?,
        ~className: option(string)=?,
        children
      ) =>
    ReasonReact.wrapJsForReason(
      ~reactClass=Config.element,
      ~props={
        "onChange": onChange |> Js.Undefined.from_opt,
        "onBlur": Js.Undefined.from_opt(onBlur),
        "onFocus": Js.Undefined.from_opt(onFocus),
        "placeholder": Js.Undefined.from_opt(placeholder),
        "className": Js.Undefined.from_opt(className)
      },
      children
    );
};

module CardElement =
  MakeElement(
    {
      [@bs.module "react-stripe-elements"] external element : ReasonReact.reactClass =
        "CardElement";
    }
  );

module CardNumberElement =
  MakeElement(
    {
      [@bs.module "react-stripe-elements"] external element : ReasonReact.reactClass =
        "CardNumberElement";
    }
  );

module CardExpiryElement =
  MakeElement(
    {
      [@bs.module "react-stripe-elements"] external element : ReasonReact.reactClass =
        "CardExpiryElement";
    }
  );

module CardCVCElement =
  MakeElement(
    {
      [@bs.module "react-stripe-elements"] external element : ReasonReact.reactClass =
        "CardCVCElement";
    }
  );

module PostalCodeElement =
  MakeElement(
    {
      [@bs.module "react-stripe-elements"] external element : ReasonReact.reactClass =
        "PostalCodeElement";
    }
  );

module PaymentRequestButtonElement = {
  [@bs.module "react-stripe-elements"] external reactClass : ReasonReact.reactClass =
    "PaymentRequestButtonElement";
  let make = (~paymentRequest, children) =>
    ReasonReact.wrapJsForReason(~reactClass, ~props={"paymentRequest": paymentRequest}, children);
};

module PaymentRequest = {
  type state = (Stripe.PaymentRequest.t, status)
  and status =
    | Loading
    | CanMakePayment(Stripe.PaymentRequest.t)
    | Loaded(Stripe.PaymentRequest.sourcePaymentResponse(Source.t))
    | Failed;
  type action =
    | CheckCanMakePayment
    | CanMakePayment
    | CanNotMakePayment
    | PaymentResponse(Stripe.PaymentRequest.sourcePaymentResponse(Source.t));
  let component = ReasonReact.reducerComponent("PaymentRequest");
  let make = (~render, ~stripe, ~options, _children) => {
    ...component,
    initialState: () => (Stripe.PaymentRequest.make(~options, stripe), Loading),
    didMount: ({reduce, state: (paymentRequest, _)}) => {
      Stripe.PaymentRequest.onSource(
        paymentRequest,
        reduce((paymentResponse) => PaymentResponse(paymentResponse))
      )
      |> ignore;
      reduce(() => CheckCanMakePayment, ());
      ReasonReact.NoUpdate
    },
    reducer: (action: action, (paymentRequest, _): state) =>
      switch action {
      | CheckCanMakePayment =>
        ReasonReact.SideEffects(
          (
            ({reduce}) =>
              Stripe.PaymentRequest.canMakePayment(paymentRequest)
              |> Js.Promise.then_(
                   (result) => {
                     switch (Js.Null.to_opt(result)) {
                     | Some(_) => reduce(() => CanMakePayment, ())
                     | None => reduce(() => CanNotMakePayment, ())
                     };
                     Js.Promise.resolve()
                   }
                 )
              |> ignore
          )
        )
      | CanMakePayment => ReasonReact.Update((paymentRequest, CanMakePayment(paymentRequest)))
      | PaymentResponse(paymentResponse) =>
        ReasonReact.Update((paymentRequest, Loaded(paymentResponse)))
      | CanNotMakePayment => ReasonReact.Update((paymentRequest, Failed))
      },
    render: ({state: (_, state)}) => render(state)
  };
};
