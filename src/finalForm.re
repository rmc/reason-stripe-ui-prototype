module FormErrors = {
  type t = Js.Undefined.t(Js.Dict.t(string));
  type formErrorSymbol = string;
  [@bs.module "final-form"] external formErrorSymbol : formErrorSymbol = "FORM_ERROR";
  let setSubmissionError = (submissionError, errors) =>
    Js.Dict.set(errors, formErrorSymbol, submissionError);
  let setFieldErrors = (fieldErrors, errors) =>
    fieldErrors |> List.iter(((fieldName, error)) => Js.Dict.set(errors, fieldName, error));
  let make = (~submissionError=?, ~fieldErrors=?, ()) =>
    switch (submissionError, fieldErrors) {
    | (Some(submissionError), Some(fieldErrors)) =>
      let errors = Js.Dict.empty();
      setSubmissionError(submissionError, errors);
      setFieldErrors(fieldErrors, errors);
      Js.Undefined.return(errors)
    | (Some(submissionError), None) =>
      let errors = Js.Dict.empty();
      setSubmissionError(submissionError, errors);
      Js.Undefined.return(errors)
    | (None, Some(fieldErrors)) =>
      let errors = Js.Dict.empty();
      setFieldErrors(fieldErrors, errors);
      Js.Undefined.return(errors)
    | (None, None) => Js.Undefined.empty
    };
};

module FormState = {
  type t;
  [@bs.get] external active : t => Js.Undefined.t(string) = "";
  [@bs.get] external dirty : t => Js.boolean = "";
  [@bs.get] external dirtySinceLastSubmit : t => Js.boolean = "";
  [@bs.get] external error : t => Js.Undefined.t(string) = "";
  [@bs.get] external errors : t => Js.Undefined.t('fieldErrors) = "";
  [@bs.get] external initialValues : t => Js.Undefined.t('partialValues) = "";
  [@bs.get] external invalid : t => Js.boolean = "";
  [@bs.get] external pristine : t => Js.boolean = "";
  [@bs.get] external submitError : t => Js.Undefined.t(string) = "";
  [@bs.get] external submitErrors : t => Js.Undefined.t('fieldErrors) = "";
  [@bs.get] external submitFailed : t => Js.boolean = "";
  [@bs.get] external submitSucceeded : t => Js.boolean = "";
  [@bs.get] external submitting : t => Js.boolean = "";
  [@bs.get] external valid : t => Js.boolean = "";
  [@bs.get] external validating : t => Js.boolean = "";
  [@bs.get] external values : t => 'values = "";
};

module Form = {
  [@bs.module "react-final-form"] external reactClass : ReasonReact.reactClass = "Form";
  module FormRenderProps = {
    include FormState;
    [@bs.get] external handleSubmit : t => 'submitHandler = "";
    [@bs.send] external getState : (t, unit) => FormState.t = "";
    [@bs.send] external initialize : (t, 'values) => unit = "";
    [@bs.send] external reset : t => unit = "";
  };
  type render = FormRenderProps.t => ReasonReact.reactElement;
  type onSubmitSync('values, 'formApi, 'errorObj) = ('values, 'formApi) => FormErrors.t;
  type onSubmitAsyncPromise('values, 'formApi, 'errorObj) =
    ('values, 'formApi) => Js.Promise.t(FormErrors.t);
  type onSubmitAsyncCallback('values, 'formApi, 'errorObj) =
    ('values, 'formApi, unit => FormErrors.t) => unit;
  type onSubmit;
  external onSubmitSyncCast : onSubmitSync('values, 'formApi, 'errorObj) => onSubmit = "%identity";
  external onSubmitAsyncPromiseCast :
    onSubmitAsyncPromise('values, 'formApi, 'errorObj) => onSubmit =
    "%identity";
  external onSubmitAsyncCallbackCast :
    onSubmitAsyncCallback('values, 'formApi, 'errorObj) => onSubmit =
    "%identity";
  let castOnSubmit = (onSubmit) =>
    switch onSubmit {
    | `Sync(onSubmit) => onSubmit |> onSubmitSyncCast
    | `AsyncPromise(onSubmitAsync) => onSubmitAsync |> onSubmitAsyncPromiseCast
    | `AsyncCallback(onSubmitAsyncCallback) => onSubmitAsyncCallback |> onSubmitAsyncCallbackCast
    };
  /* validate?: (values: Object) => Object | Promise<Object>
     A whole-record validation function that takes all the values of the form and returns any validation errors. There are three possible ways to write a validate function:

     Synchronously: returns {} or undefined when the values are valid, or an Object of validation errors when the values are invalid.
     Asynchronously with a Promise: returns a Promise<?Object> that resolves with no value on success or resolves with an Object of validation errors on failure. The reason it resolves with errors is to leave rejection for when there is a server or communications error.
     Validation errors must be in the same shape as the values of the form. You may return a generic error for the whole form using the special FORM_ERROR symbol key.

     An optional callback for debugging that returns the form state and the states of all the fields. It's called on every state change. A typical thing to pass in might be console.log. */
  type validate('values, 'errorObj) = 'values => Js.Undefined.t(Js.t('errorObj));
  let make =
      (
        ~initialValues: option('partialValues)=?,
        ~render: render,
        ~onSubmit,
        ~validate: option(validate('values, 'errorObj))=?,
        ~validateOnBlur=?,
        ~subscription: option(list(string))=?,
        children
      ) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      ~props=
        Js.Undefined.(
          {
            "initialValues": initialValues |> from_opt,
            "render": render,
            "onSubmit": onSubmit |> castOnSubmit,
            "validate": validate |> from_opt,
            "validateOnBlur": validateOnBlur |> Utils.unwrapBool,
            "subscription": subscription |> Utils.mapOpt(Array.of_list) |> from_opt
          }
        ),
      children
    );
};

module Field = {
  [@bs.module "react-final-form"] external reactClass : ReasonReact.reactClass = "Field";
  module FieldRenderProps = {
    type t;
    type onChange('value) = 'value => unit;
    [@bs.scope "input"] [@bs.get] external name : t => string = "";
    [@bs.scope "input"] [@bs.get] external value : t => 'value = "";
    [@bs.scope "input"] [@bs.get] external onChange : t => onChange('value) = "";
    [@bs.scope "input"] [@bs.get] external onBlur : t => [@bs] (ReactEventRe.Focus.t => unit) = "";
    [@bs.scope "input"] [@bs.get] external onFocus : t => [@bs] (ReactEventRe.Focus.t => unit) =
      "";
    [@bs.scope "meta"] [@bs.get] external active : t => Js.boolean = "";
    [@bs.scope "meta"] [@bs.get] external data : t => 'data = "";
    [@bs.scope "meta"] [@bs.get] external dirty : t => Js.boolean = "";
    [@bs.scope "meta"] [@bs.get] external error : t => Js.Undefined.t('error) = "";
    [@bs.scope "meta"] [@bs.get] external initial : t => Js.Undefined.t('value) = "";
    [@bs.scope "meta"] [@bs.get] external invalid : t => Js.boolean = "";
    [@bs.scope "meta"] [@bs.get] external pristine : t => Js.boolean = "";
    [@bs.scope "meta"] [@bs.get] external submitError : t => Js.Undefined.t('error) = "";
    [@bs.scope "meta"] [@bs.get] external submitFailed : t => Js.boolean = "";
    [@bs.scope "meta"] [@bs.get] external submitSucceeded : t => Js.boolean = "";
    [@bs.scope "meta"] [@bs.get] external touched : t => Js.boolean = "";
    [@bs.scope "meta"] [@bs.get] external valid : t => Js.boolean = "";
    [@bs.scope "meta"] [@bs.get] external visited : t => Js.boolean = "";
  };
  type render = FieldRenderProps.t => ReasonReact.reactElement;
  let make = (~name: string, ~render, children) =>
    ReasonReact.wrapJsForReason(~reactClass, ~props={"render": render, "name": name}, children);
};

module FormSpy = {
  [@bs.module "react-final-form"] external reactClass : ReasonReact.reactClass = "FormSpy";
  type onChange('subscribedFormState) = 'subscribedFormState => unit;
  let make =
      (
        ~onChange: option(onChange('subscribedFormState))=?,
        ~render: 'subscribedFormState => ReasonReact.reactElement,
        children
      ) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      ~props={"render": render, "onChange": onChange |> Js.Undefined.from_opt},
      children
    );
};
