module ApolloLink = {
  open ReasonApolloTypes;
  [@bs.module "apollo-link"]
  external split : (Js.t('a) => Js.boolean, apolloLink, apolloLink) => apolloLink =
    "";
};

module ApolloUtilities = {
  [@bs.module "apollo-utilities"]
  external getMainDefinition : 'query => {. "kind": string, "operation": string} =
    "";
};

module ApolloClient = {
  include ReasonApolloTypes;
  type generatedApolloClient = {. "query": [@bs.meth] (ApolloClient.queryObj => string)};
  module Cast = (ApolloClientCast: ApolloClient.ApolloClientCast) => {
    include ApolloClient.Cast(ApolloClientCast);
    type subscriptionObj = {. "query": queryString, "variables": ApolloClientCast.variables};
    external castClient :
      ApolloClient.generatedApolloClient =>
      {
        .
        "query": [@bs.meth] (queryObj => string),
        "mutate": [@bs.meth] (mutationObj => string),
        "subscribe": [@bs.meth] (subscriptionObj => Observable.t(string))
      } =
      "%identity";
    [@bs.obj]
    external getJSSubscriptionConfig :
      (~query: queryString, ~variables: ApolloClientCast.variables=?, unit) => subscriptionObj =
      "";
  };
};

module ReasonApolloSubscription = {
  module type InternalConfig = {let apolloClient: ApolloClient.generatedApolloClient;};
  module type ClientConfig = {
    type response;
    type variables;
    let subscription: ReasonApolloTypes.queryString;
  };
  module Make = (InternalConfig: InternalConfig, ClientConfig: ClientConfig) => {
    module CastApolloClient =
      ApolloClient.Cast(
        {
          type variables = ClientConfig.variables;
        }
      );
    let apolloClient = CastApolloClient.castClient(InternalConfig.apolloClient);
    external cast : string => {. "data": ClientConfig.response} = "%identity";
    type unsubscribe = unit => unit;
    type state = {
      status,
      subscription: ref(option(unsubscribe))
    }
    and status =
      | NotSubscribed
      | Subscribing
      | Subscribed([ | `Empty | `Data(ClientConfig.response)])
      | Failed(string);
    type action =
      | Subscribe
      | Unsubscribe
      | Start(unsubscribe)
      | Next(string)
      | Error_(string);
    type retainedProps = {variables: option(ClientConfig.variables)};
    let subscribe =
        (~query, ~variables=?, {state, send}: ReasonReact.self(state, retainedProps, action)) => {
      switch state.subscription^ {
      | Some(unsubscribe) => unsubscribe()
      | _ => ()
      };
      let subscriptionConfig =
        switch variables {
        | Some(variables) => CastApolloClient.getJSSubscriptionConfig(~query, ~variables, ())
        | None => CastApolloClient.getJSSubscriptionConfig(~query, ())
        };
      apolloClient##subscribe(subscriptionConfig)
      |> Observable.subscribe(
           ~next=(value) => send(Next(value)),
           ~error=(error) => send(Error_(error))
         )
      |> ((unsubscribe) => send(Start(unsubscribe)))
    };
    let component = ReasonReact.reducerComponentWithRetainedProps("ReasonApolloSubscription");
    let make = (~variables=?, ~subscribeOnMount=true, children) => {
      ...component,
      initialState: () => {status: NotSubscribed, subscription: ref(None)},
      reducer: (action: action, state: state) =>
        switch (action: action, state.subscription^) {
        | (Subscribe, Some(_)) => ReasonReact.NoUpdate
        | (Subscribe, None) =>
          ReasonReact.UpdateWithSideEffects(
            {...state, status: Subscribed(`Empty)},
            ((self) => subscribe(~query=ClientConfig.subscription, ~variables?, self))
          )
        | (Start(unsubscribe), _) =>
          state.subscription := Some(unsubscribe);
          ReasonReact.NoUpdate
        | (Next(value), _) =>
          let typedData = cast(value)##data;
          ReasonReact.Update({...state, status: Subscribed(`Data(typedData))})
        | (Error_(error), _) => ReasonReact.Update({...state, status: Failed(error)})
        | (Unsubscribe, Some(unsubscribe)) => ReasonReact.SideEffects(((_) => unsubscribe()))
        | (Unsubscribe, None) => ReasonReact.NoUpdate
        },
      retainedProps: {variables: variables},
      didUpdate: ({oldSelf, newSelf}) =>
        switch (oldSelf.retainedProps.variables, newSelf.retainedProps.variables) {
        | (Some(retainedVariables), Some(variables)) when variables !== retainedVariables =>
          newSelf.send(Subscribe)
        | _ => ()
        },
      didMount: ({send}) => {
        switch subscribeOnMount {
        | true => send(Subscribe)
        | _ => ()
        };
        ReasonReact.NoUpdate
      },
      willUnmount: ({send}) => send(Unsubscribe),
      render: ({state, send}) => {
        let subscribe = () => send(Subscribe);
        let unsubscribe = () => send(Unsubscribe);
        children[0](state.status, subscribe, unsubscribe)
      }
    };
  };
};

module ReasonApolloMutationFunc = {
  module type InternalConfig = {let apolloClient: ApolloClient.generatedApolloClient;};
  module type ClientConfig = {
    type response;
    type variables;
    let mutation: ReasonApolloTypes.queryString;
  };
  module Make = (InternalConfig: InternalConfig, ClientConfig: ClientConfig) => {
    module CastApolloClient =
      ApolloClient.Cast(
        {
          type variables = ClientConfig.variables;
        }
      );
    let apolloClient = CastApolloClient.castClient(InternalConfig.apolloClient);
    external cast : string => {. "data": ClientConfig.response, "loading": bool} = "%identity";
    let mutation = ClientConfig.mutation;
    let make = (~variables=?, ()) =>
      Js.Promise.(
        resolve(
          apolloClient##mutate(
            switch variables {
            | Some(variables) => CastApolloClient.getJSMutationConfig(~mutation, ~variables, ())
            | None => CastApolloClient.getJSMutationConfig(~mutation, ())
            }
          )
        )
        |> then_((value) => resolve(cast(value)##data))
      );
  };
};

/*
 * Extend CreateClient to support Subscriptions */
module CreateClient = (Config: ReasonApollo.ApolloClientConfig) => {
  include ReasonApollo.CreateClient(Config);
  /*
   * Expose a module to perform "subscription" operations for the given client
   */
  module Subscription =
    ReasonApolloSubscription.Make(
      {
        let apolloClient = apolloClient;
      }
    );
  module MutationFunc =
    ReasonApolloMutationFunc.Make(
      {
        let apolloClient = apolloClient;
      }
    );
};

module type SubscriptionLinkConfig = {let uri: string; let reconnect: bool;};

module CreateSubscriptionLink = (Config: SubscriptionLinkConfig) => {
  open ReasonApolloTypes;
  type wsLinkOptions = {. "uri": string, "options": {. "reconnect": Js.boolean}};
  [@bs.module "apollo-link-ws"] [@bs.new] external createWsLink : wsLinkOptions => apolloLink =
    "WebSocketLink";
  let link =
    createWsLink({
      "uri": Config.uri,
      "options": {"reconnect": Js.Boolean.to_js_boolean(Config.reconnect)}
    });
};

module InMemoryCache =
  ApolloInMemoryCache.CreateInMemoryCache(
    {
      type dataObject;
      let inMemoryCacheObject = Js.Nullable.undefined;
    }
  );

module HttpLink =
  ApolloLinks.CreateHttpLink(
    {
      let uri = "https://apistripe.localtunnel.me/graphql";
    }
  );

module SubscriptionLink =
  CreateSubscriptionLink(
    {
      let uri = "ws://localhost:8888/subscriptions";
      let reconnect = true;
    }
  );

module ClientConfig = {
  let apolloClient =
    ReasonApollo.createApolloClient(
      ~cache=InMemoryCache.cache,
      ~link=
        ApolloLink.split(
          (config) => {
            let mainDefinition = config##query |> ApolloUtilities.getMainDefinition;
            switch (mainDefinition##kind, mainDefinition##operation) {
            | ("OperationDefinition", "subscription") => Js.true_
            | _ => Js.false_
            }
          },
          SubscriptionLink.link,
          HttpLink.link
        ),
      ()
    );
};

module Client = CreateClient(ClientConfig);
