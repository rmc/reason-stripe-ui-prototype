let getStyleProp = (key: string, style: ReactDOMRe.Style.t) => {
  let st: Js.Dict.t('a) = Obj.magic(style);
  Js.Dict.get(st, key)
};

let mapOpt = (f, value) =>
  switch value {
  | Some(x) => Some(f(x))
  | _ => None
  };

let chainOpt = (f, value) =>
  switch value {
  | Some(x) => f(x)
  | _ => None
  };

let defaultOpt = (default, value) =>
  switch value {
  | Some(x) => x
  | None => default
  };

let unwrapBool = (value) => value |> mapOpt(Js.Boolean.to_js_boolean) |> Js.Undefined.from_opt;
