let component = ReasonReact.statelessComponent("App");

let make = (_children) => {
  ...component,
  render: (_self) =>
    <div>
      <MaterialUi.Styles.MuiThemeProvider
        theme=(MaterialUi.Styles.createMuiTheme({"palette": {"type": "light"}}))>
        <MaterialUi.Reboot />
        <Tenant.TenantQuery>
          (
            (tenantQueryResult) =>
              switch tenantQueryResult {
              | Tenant.TenantQuery.Loading =>
                <div> (ReasonReact.stringToElement("loading tenant")) </div>
              | Tenant.TenantQuery.Failed(error) =>
                <div>
                  (ReasonReact.stringToElement("failed fetching tenant:"))
                  <pre> (ReasonReact.stringToElement(error)) </pre>
                  Routing.linkHome
                </div>
              | Tenant.TenantQuery.Loaded(response) =>
                let apiKey = response##tenant##fundraising##stripe##publishableKey;
                let stripeAccount = response##tenant##fundraising##stripe##connectedAccountId;
                /* TODO: let stripeAccountCountry = response##tenant##fundraising##stripe##country; */
                let stripeAccountCountry = "DE";
                <Stripe.Provider apiKey stripeAccount>
                  <Stripe.Elements>
                    <Routing.Switch
                      render=(
                        (route) =>
                          switch route {
                          | Routing.Routes.Form(paymentMethod) =>
                            <Stripe.InjectStripe
                              render=(
                                (props) =>
                                  <Form paymentMethod stripe=props##stripe stripeAccountCountry />
                              )
                            />
                          | Routing.Routes.Pending(donationId, type_) =>
                            <Donation.DonationQuery variables={"id": donationId}>
                              (
                                (donationQueryState) =>
                                  switch donationQueryState {
                                  | Donation.DonationQuery.Failed(error) =>
                                    <div>
                                      (ReasonReact.stringToElement("failed fetching donation:"))
                                      <pre> (ReasonReact.stringToElement(error)) </pre>
                                    </div>
                                  | Donation.DonationQuery.Loading =>
                                    <div> (ReasonReact.stringToElement("loading donation")) </div>
                                  | Donation.DonationQuery.Loaded(data) =>
                                    <Donation.DonationSubscription variables={"id": donationId}>
                                      (
                                        (subscriptionState, _subscribe, _unsubscribe) => {
                                          let (subscriptionStateString, donation) =
                                            switch subscriptionState {
                                            | Donation.DonationSubscription.NotSubscribed => (
                                                "not subscribed",
                                                data##donation
                                              )
                                            | Donation.DonationSubscription.Subscribing => (
                                                "subscribing",
                                                data##donation
                                              )
                                            | Donation.DonationSubscription.Subscribed(`Empty) => (
                                                "subscribed",
                                                data##donation
                                              )
                                            | Donation.DonationSubscription.Subscribed(
                                                `Data(subscriptionData)
                                              ) => (
                                                "subscribed (got data)",
                                                subscriptionData##donationUpdated##donation
                                              )
                                            | Donation.DonationSubscription.Failed(error) => (
                                                "failed: " ++ error,
                                                data##donation
                                              )
                                            };
                                          <Pending
                                            donation
                                            subscriptionState=subscriptionStateString
                                            type_
                                          />
                                        }
                                      )
                                    </Donation.DonationSubscription>
                                  }
                              )
                            </Donation.DonationQuery>
                          | Routing.Routes.Redirect({clientSecret, sourceId}) =>
                            <Stripe.InjectStripe
                              render=(
                                (props) =>
                                  <RedirectHandler clientSecret sourceId stripe=props##stripe />
                              )
                            />
                          | Routing.Routes.External(redirectUrl) =>
                            <a href=redirectUrl> (ReasonReact.stringToElement(redirectUrl)) </a>
                          }
                      )
                    />
                  </Stripe.Elements>
                </Stripe.Provider>
              }
          )
        </Tenant.TenantQuery>
      </MaterialUi.Styles.MuiThemeProvider>
    </div>
};
