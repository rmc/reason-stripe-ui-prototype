type t;

[@bs.module "query-string"] external parse : string => Js.t('a) = "";

[@bs.module "query-string"] external stringify : Js.t('a) => string = "";
